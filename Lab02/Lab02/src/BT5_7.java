import java.util.Arrays;

public class BT5_7 {
    public static void main(String[] args) {
        int[][] matrix1 = { {1,4,3}, {2,4,1}, {2, 8, 1}};
        int[][] matrix2 = { {0,2,1}, {1,0,1}, {9, 0, 6}};
        int[][] sum = new int[3][3];

        System.out.println("Matrix 1:");
        for (int i = 0; i < matrix1.length; i++) {
            System.out.println(Arrays.toString(matrix1[i]));
        }
        System.out.println("Matrix 2:");
        for (int i = 0; i < matrix2.length; i++) {
            System.out.println(Arrays.toString(matrix2[i]));
        }

        for (int i = 0; i < sum.length; i++) {
            for (int k = 0; k < sum.length; k++) {
                sum[i][k] = matrix1[i][k] + matrix2[i][k];
            }
        }

        System.out.println("Sum: ");
        for (int i = 0; i < sum.length; i++) {
            System.out.println(Arrays.toString(sum[i]));
        }
    }
}
