import java.util.Scanner;

public class DaysOfMonth {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String month, StrYear;
        int IntYear;

        String[] MONTH = {"1",          "2",        "3",     "4",    "5",    "6",   "7",     "8",        "9",        "10",      "11",       "12",
                          "January", "February",  "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
                          "Jan.",     "Feb.",      "Mar.", "Apr.",                         "Aug.",   "Sept.",     "Oct.",    "Nov.",     "Dec.", 
                          "Jan" ,     "Feb",       "Mar",  "Apr",          "Jun",  "Jul",  "Aug",    "Sep",       "Oct",     "Nov",      "Dec"};

        boolean invalidMonth = false;

        do {
            System.out.println("Month: ");
            month = sc.nextLine();
            for(int i = 0; i < MONTH.length; i++){
                if(MONTH[i].equals(month)){
                    invalidMonth = true;
                    break;
                }
            }
            System.out.println("Year: ");
            StrYear = sc.nextLine();
            if(invalidMonth == false) {
                System.out.println("Invalid month "+ month);
            }
            IntYear = convert(StrYear);
        } while( invalidMonth == false ||  IntYear == 0 );

        switch(month) {
            case "1":       case "3":     case "5":   case "7":    case "8":      case "10":      case "12":
            case "January": case "March": case "May": case "July": case "August": case "October": case "December":
            case "Jan.":    case "Mar.":                           case "Aug.":   case "Oct.":    case "Dec.":
            case "Jan":     case "Mar":               case "Jul":  case "Aug":    case "Oct":     case "Dec":
                System.out.println("This month has 31 days");
                break;
            case "4":     case "6":    case "9":         case "11":
            case "April": case "June": case "September": case "November":
            case "Apr.":               case "Sept.":     case "Nov.":
            case "Apr":   case "Jun":  case "Sep":       case "Nov": 
                System.out.println("This month has 30 days");
                break;
            case "2": case "February": case "Feb.": case "Feb":
                if(IntYear%400 == 0 || (IntYear%4 == 0 && IntYear%100 != 0 )) {
                    System.out.println("This month has 29 days");
                } else {
                    System.out.println("This month has 28 days");
                }
                break;
        }
        sc.close();
        System.exit(0);
    }
    
    public static int convert(String str)
    {
        int val = 0;
        try {
            val = Integer.parseInt(str);
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid Year " + str);
        }
        return val;
    }
}
