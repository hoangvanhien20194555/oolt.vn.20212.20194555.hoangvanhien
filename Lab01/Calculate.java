import javax.swing.JOptionPane;

public class Calculate {
    public static void main(String[] args) {
        double num1 = Double.parseDouble(JOptionPane.showInputDialog(null,"Enter the first number: ","Input first number", JOptionPane.INFORMATION_MESSAGE));
        double num2 = Double.parseDouble(JOptionPane.showInputDialog(null,"Enter the second number: ","Input second number", JOptionPane.INFORMATION_MESSAGE));

        String result = "Sum: " + (num1 + num2) + "\nDifference: " + (num1 - num2) + "\nProduct: " + (num1*num2) + "\nQuotient: ";

        if(num2 == 0){
            result += "Error! The second number mustn't be 0!";
        }
        else {
            result += (num1/num2);
        }

        JOptionPane.showMessageDialog(null,result, "Show result", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}
