import javax.swing.JOptionPane;

public class ShowTwoNumbers {
    public static void main(String[] args) {
        String num1, num2;

        num1 = JOptionPane.showInputDialog(null,"Enter the first number: ","Input first number", JOptionPane.INFORMATION_MESSAGE);
        num2 = JOptionPane.showInputDialog(null,"Enter the second number: ","Input second number", JOptionPane.INFORMATION_MESSAGE);

        String result = "You entered: " + num1 + " and " + num2;

        JOptionPane.showMessageDialog(null, result, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}