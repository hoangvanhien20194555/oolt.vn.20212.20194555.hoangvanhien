package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

import Disc.Track;

public class CompactDisc extends Disc implements Playable{
    private String artist;
    private List<Track> tracks = new ArrayList<>();
    public String getArtist() {
        return artist;
    }
    public void setArtist(String artist) {
        this.artist = artist;
    }
   public int getLength(ArrayList<Track> tracks){
       int sumLength = 0;
       for(int i = 0; i < tracks.size(); i++){
           sumLength += tracks.get(i).getLength();
       }
       super.length = sumLength;
       return super.length;
   }

   public void addTrack(Track track){
       if(tracks.contains(track)){
           System.out.println("Already exist.");
       }
       else{
           tracks.add(track);
       }
   }

   public void removeTrack(Track track){
       if(tracks.contains(track)){
           tracks.remove(track);
       }
       else{
           System.out.print("Does not exist.");
       }
   }
   public CompactDisc(String director, int length, String artist, List<Track> tracks) {
        super(director, length);
        this.artist = artist;
        this.tracks = tracks;
   }
public CompactDisc(String id, String title, String category, String artist2, float cost) {
}
@Override
public void play() {
    // show CD info:
    System.out.println("Tile: " + this.getTitle());
    System.out.println("Category: " + this.getCategory());
    System.out.println("Cost: " + this.getCost());
    System.out.println("Director: " + this.getDirector());
    System.out.println("Artist: " + this.getArtist());
    for(int i = 0; i < tracks.size(); i++){
        tracks.get(i).play(); // câu này tương đương với 2 câu dưới
        // System.out.println("Playing track: " + tracks.get(i).getTitle());
        // System.out.println("Track length: " + tracks.get(i).getLength());
    }
}
    
    
}
