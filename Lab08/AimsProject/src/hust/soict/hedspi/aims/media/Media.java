package hust.soict.hedspi.aims.media;

public abstract class Media {
    protected static String title;
    protected static String category;
    protected static float cost;
    protected static String id;
	
	public Media(String id, String title, float cost) {
		this.id = id;
		this.title = title;
		this.cost = cost;
	}
	
	public Media(String id, String title, String category, float cost) {
		this(id, title, cost);
		Media.category = category;
	}
    
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        Media.title = title;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        Media.category = category;
    }
    public float getCost() {
        return cost;
    }
    public void setCost(float cost) {
        Media.cost = cost;
    }
    public String getId() {
		return id;
	}
    public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if( obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Media other = (Media) obj;
		if (id != other.id){
			return false;
		};
		return true;
	}
	public int compareTo(Media media) {
		if (this instanceof Book && (media instanceof DigitalVideoDisc || media instanceof CompactDisc)) {
			return -1;
		}
		if(this instanceof DigitalVideoDisc && media instanceof CompactDisc)
			return -1;
		if(this instanceof CompactDisc && (media instanceof DigitalVideoDisc || media instanceof Book))
			return 1;
		if(this instanceof DigitalVideoDisc && media instanceof Book)
			return 1;
		return 0;
	}

}
