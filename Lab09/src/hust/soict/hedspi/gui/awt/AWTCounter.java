package hust.soict.hedspi.gui.awt;

import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

public class AWTCounter extends Frame implements ActionListener {
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count;

    //Constructor cho AWTCounter: khoi tao cua so giao dien
    public AWTCounter(){
        //thiet lap layout cho cua so giao dien
        this.setLayout(new FlowLayout());

        //khoi tao cac component/ control
        lblCount = new Label("Counter");
        //Them vao cua so giao dien
        this.add(lblCount);

        tfCount = new TextField(count + " ", 10);
        tfCount.setEditable(false);
        this.add(tfCount);

        btnCount = new Button("Count");
        this.add(btnCount);

        //Dang ky lang nghe su kien
        btnCount.addActionListener(this);

        //Thiet lap thong tin cho cua so giao dien
        this.setTitle("AWT Counter");
        this.setSize(250, 100);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        new AWTCounter();
    }

    @Override
    public void actionPerformed(ActionEvent e){
        ++count;
        this.tfCount.setText(count + "");
    }
}
