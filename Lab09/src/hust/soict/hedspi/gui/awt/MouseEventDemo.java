package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class MouseEventDemo extends Frame{
    private TextField tfMouseX;
    private TextField tfMouseY;
    public MouseEventDemo(){
        setLayout(new FlowLayout());

        add(new Label("X-Click: "));

        tfMouseX = new TextField(10);
        tfMouseX.setEditable(false);
        add(tfMouseX);

        add(new Label("Y-Click: "));

        tfMouseY = new TextField(10);
        tfMouseY.setEditable(false);
        add(tfMouseY);

        addMouseListener(new MyMouseListener());

        setTitle("MouseEvent Demo");
        setSize(350, 100);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MouseEventDemo();
    }

    private class MyMouseListener implements MouseListener {

        @Override
        public void mouseClicked(java.awt.event.MouseEvent e) {
            tfMouseX.setText(e.getX() + "");
            tfMouseY.setText(e.getX() + "");
            
        }

        @Override
        public void mousePressed(java.awt.event.MouseEvent e) {
            
        }

        @Override
        public void mouseReleased(java.awt.event.MouseEvent e) {
            
        }

        @Override
        public void mouseEntered(java.awt.event.MouseEvent e) {
            
        }

        @Override
        public void mouseExited(java.awt.event.MouseEvent e) {
            
        }
        
    }
}
