package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class KeyEventDemo extends Frame{
    private TextField tfInput;
    private TextArea taDisplay;

    public KeyEventDemo(){
        setLayout(new FlowLayout());

        add(new Label("Enter Text: "));
        tfInput = new TextField();
        add(tfInput);
        taDisplay = new TextArea(5, 40);
        add(taDisplay);

        tfInput.addKeyListener(new MyKeyListener());

        setTitle("KeyEvent Demo");
        setSize(400, 200);
        setVisible(true);
    }

    public static void main(String[] args) {
        new KeyEventDemo();
    }

    private class MyKeyListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            taDisplay.append("You have typed " + e.getKeyChar() + "\n");
        }

        @Override
        public void keyPressed(KeyEvent e) {
            
        }

        @Override
        public void keyReleased(KeyEvent e) {
            
        }
        
    }

}
