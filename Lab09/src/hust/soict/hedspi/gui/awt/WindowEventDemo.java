package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class WindowEventDemo extends Frame {
    private TextField tfCount;
    private Button btnCount;
    private int count = 0;

    public WindowEventDemo(){
        setLayout(new FlowLayout());

        add(new Label("Counter"));

        tfCount = new TextField("0", 10);
        tfCount.setEditable(false);
        add(tfCount);

        btnCount = new Button("Count");
        add(btnCount);

        btnCount.addActionListener(new BtnCountListener());

        addWindowListener(new MyWindowListener());

        setTitle("WindowEvent Demo");
        setSize(300, 100);
        setVisible(true);
    }

    public static void main(String[] args) {
        new WindowEventDemo();
    }

    private class BtnCountListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ++count;
            tfCount.setText(count + "");
            
        }
    }
    private class MyWindowListener implements WindowListener{

        @Override
        public void windowOpened(WindowEvent e) {
            
        }

        @Override
        public void windowClosing(WindowEvent e) {
           System.exit(0);
        }

        @Override
        public void windowClosed(WindowEvent e) {
           
        }

        @Override
        public void windowIconified(WindowEvent e) {
            System.out.println("Window Iconified");
            
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
            System.out.println("Window Deiconified");
            
        }

        @Override
        public void windowActivated(WindowEvent e) {
            System.out.println("Window Activated");
            
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
            System.out.println("Window Deactivated");
            
        }
        
    }
}
