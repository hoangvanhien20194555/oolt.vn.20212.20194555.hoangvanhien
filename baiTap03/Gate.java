

public class Gate {

	private String name;

	private int distance;

	public Gate(String name, int distance) {
		this.name = name;
		this.distance = distance;
	}

	public void enter(Ticket ticket) {
		if(ticket.isValid()) {
			open();
			ticket.setOrigin(this);
		} else {
			System.out.println("Ticket invalid!");
			close();	
		}
	}

	public void exit(Ticket ticket) {
		int value = Line.getFare(distance);
		if(ticket.getValue() < value || ticket.getValue() < 9000) {
			System.out.println("Ve khong du tien!");
			close();
		} else {
			ticket.adjustValue(value);
			System.out.println("Da thanh toan!");
			open();
		}
	}

	public void open() {
		System.out.println("Open gate " + name + "!");
	}

	public void close() {
		System.out.println("Close gate " + name + "!");
	}

}
