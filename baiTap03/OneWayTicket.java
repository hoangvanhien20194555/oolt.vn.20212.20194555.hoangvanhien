import java.util.Date;

public class OneWayTicket extends Ticket {

	private boolean valid = true;

	public OneWayTicket(int value, Date date) {
		super(value, date);
	}

	public void setOrigin(Gate gate) {
		this.gate = gate;
	}

	public boolean isValid() {
		if(issuedDate.before((new Date()))) {
			return valid;
		} 
		return false;
	}

}
