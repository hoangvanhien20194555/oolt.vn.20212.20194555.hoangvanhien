import java.util.Date;

public class test {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Date future = new Date(); // neu issuedDate trong tuong lai -> invalid
		future.setDate(10000);
		Date past = new Date(); // issuedDate trong qua khu -> valid
		past.setTime(10000);
		Line line = new Line();
		
		Ticket tk0 = new OneWayTicket(9000, future);
		Ticket tk1 = new OneWayTicket(11000, past);
		Ticket tk2 = new OneWayTicket(11000, past);
		
//		trường hợp vé không hợp lệ
		line.A.enter(tk0);
		System.out.println();
//		trường hợp vé không đủ tiền - cửa đóng tại cửa soát vé ga đến (Ga B)
		line.B.enter(tk1);
		line.B.exit(tk1);
		System.out.println();
//		trường hợp vé đủ tiền và lên được tàu
		line.C.enter(tk2);
		line.C.exit(tk2);
		System.out.println();
		
		System.out.println("----------------------------------------");
		Ticket tk3 = new PrepaidCard(8000, future);
		Ticket tk4 = new PrepaidCard(10000, past);
		Ticket tk5 = new PrepaidCard(17000, past);
		
//		 trường hợp thẻ có số tiền không đủ mức sàn - cửa đóng tại ga xuất phát B
		line.B.enter(tk3);
		System.out.println();
//		trường hợp thẻ không đủ tiền để lên tàu lên ga đến - cửa đóng tại cửa soát vé ga đến D
		line.D.enter(tk4);
		line.D.exit(tk4);
		System.out.println();
//		trường hợp thẻ đủ tiền và lên được tàu
		line.C.enter(tk5);
		line.C.exit(tk5);
		System.out.println();
	}

}
