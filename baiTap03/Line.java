
public class Line {
	public final Gate A = new Gate("A", 0);
	public final Gate B = new Gate("B", 17);
	public final Gate C = new Gate("C", 5);
	public final Gate D = new Gate("D", 11);
	
	public Line() {
	}
	public static int getFare(int distance) {
		// neu dis <= 6km thi gia la 9.000 vnd
		int plus = 0;
		if(distance <= 6) {
			return 9000;
		} else {
			// neu khoang cach lon hon 6 thi cu them <= 2km thi tang 2000
			plus = (distance - 6) / 2; // so 2km tang them
			if(((distance - 6) % 2) != 0) { // kiem tra xem quang cuoi da den 2km tiep theo chua
				return 9000 + 2000*(plus +1);
			} else return 9000 + 2000*plus;
		}
	}
}
