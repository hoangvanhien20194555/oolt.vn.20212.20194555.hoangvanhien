import java.util.Date;

public abstract class Ticket {

	protected Date issuedDate;

	protected int value;
	
	protected Gate gate = null;

	public Ticket(int value, Date date) {
		issuedDate = date;
		this.value = value;
	}
	public int getValue() {
		return this.value;
	}

	public void setOrigin(Gate gate) {
		this.gate = gate;
	}
	
	public Gate getOrigin() {
		return this.gate;
	}

	public void adjustValue(int value) {
		this.value -= value;
	}

	public boolean isValid() {
		if((new Date()).before(issuedDate)) {
			return false;
		} 
		return true;
	}
}
