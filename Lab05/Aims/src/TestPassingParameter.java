public class TestPassingParameter {
    public static void main(String[] args){
        DigitalVideoDisc fightClubDVD = new DigitalVideoDisc("Fight club");
        DigitalVideoDisc donnieDarkoDVD = new DigitalVideoDisc("Donnie Darko");

        swap(fightClubDVD, donnieDarkoDVD);
        System.out.println("Fight club dvd title : " + fightClubDVD.getTitle());
        System.out.println("Donnie Darko dvd title : " + donnieDarkoDVD.getTitle());

        changeTitle(fightClubDVD, donnieDarkoDVD.getTitle());
        System.out.println("Fight club dvd title : " + fightClubDVD.getTitle());
    }
    
    public static void swap(Object o1, Object o2) {
        Object tmp = o1;
        o1 = o2;
        o2 = tmp; 
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        dvd.setTiltle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
