public class Aims {
    public static void main(String[] args) {
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Fight club");
        dvd1.setCategory("Thriller");
        dvd1.setDirector("David Fincher");
        dvd1.setLength(139);
        dvd1.setCost(29.95f);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc(
                "Donnie Darko", "Science Fiction", "Richard Kelly", 113, 19.95f);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc(
                "Big hero 6", "Animaiton", "Chris Williams", 102, 16.95f);

        Order anOrder = new Order();

        anOrder.addDigitalVideoDisc(dvd1);
        anOrder.addDigitalVideoDisc(dvd1);
        anOrder.removeDigitalVideoDisc(dvd1);
        anOrder.removeDigitalVideoDisc(dvd2);
        anOrder.addDigitalVideoDisc(dvd3);
        anOrder.addDigitalVideoDisc(dvd3);
        anOrder.addDigitalVideoDisc(dvd3);
        anOrder.removeDigitalVideoDisc(dvd3);

        System.out.println("Total cost is: " + anOrder.totalCost());
    }
}
